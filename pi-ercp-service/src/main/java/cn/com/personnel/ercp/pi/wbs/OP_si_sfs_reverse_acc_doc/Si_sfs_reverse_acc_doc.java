/**
 * Si_sfs_reverse_acc_doc.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package cn.com.personnel.ercp.pi.wbs.OP_si_sfs_reverse_acc_doc;

public interface Si_sfs_reverse_acc_doc extends java.rmi.Remote {
    public void si_sfs_reverse_acc_doc(Dt_sfs_reverse_acc_doc mt_sfs_reverse_acc_doc) throws java.rmi.RemoteException;
}
