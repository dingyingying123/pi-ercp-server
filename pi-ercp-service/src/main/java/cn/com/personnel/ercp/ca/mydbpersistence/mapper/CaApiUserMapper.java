package cn.com.personnel.ercp.ca.mydbpersistence.mapper;

import cn.com.personnel.ercp.ca.mydbpersistence.entity.CaApiUser;
import cn.com.personnel.springboot.framework.core.persistence.BaseMapper;

public interface CaApiUserMapper extends BaseMapper<CaApiUser> {
}