/**
 * Si_sfs_bill_acc_docService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package cn.com.personnel.ercp.pi.wbs.OP_si_sfs_bill_acc_doc;

public interface Si_sfs_bill_acc_docService extends javax.xml.rpc.Service {
    public String getHTTPS_PortAddress();

    public Si_sfs_bill_acc_doc getHTTPS_Port() throws javax.xml.rpc.ServiceException;

    public Si_sfs_bill_acc_doc getHTTPS_Port(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
    public String getHTTP_PortAddress();

    public Si_sfs_bill_acc_doc getHTTP_Port() throws javax.xml.rpc.ServiceException;

    public Si_sfs_bill_acc_doc getHTTP_Port(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
