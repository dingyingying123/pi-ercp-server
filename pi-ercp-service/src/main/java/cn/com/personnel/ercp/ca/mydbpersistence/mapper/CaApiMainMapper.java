package cn.com.personnel.ercp.ca.mydbpersistence.mapper;

import cn.com.personnel.ercp.ca.mydbpersistence.entity.CaApiMain;
import cn.com.personnel.springboot.framework.core.persistence.BaseMapper;

public interface CaApiMainMapper extends BaseMapper<CaApiMain> {
}