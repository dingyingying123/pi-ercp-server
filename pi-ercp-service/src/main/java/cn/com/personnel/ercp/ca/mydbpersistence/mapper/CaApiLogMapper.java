package cn.com.personnel.ercp.ca.mydbpersistence.mapper;

import cn.com.personnel.ercp.ca.mydbpersistence.entity.CaApiLog;
import cn.com.personnel.springboot.framework.core.persistence.BaseMapper;

public interface CaApiLogMapper extends BaseMapper<CaApiLog> {
}