package cn.com.personnel.ercp.ca.mydbpersistence.mapper;

import cn.com.personnel.ercp.ca.mydbpersistence.entity.SecUserCA;
import cn.com.personnel.springboot.framework.core.persistence.BaseMapper;

public interface SecUserCAMapper extends BaseMapper<SecUserCA> {
}