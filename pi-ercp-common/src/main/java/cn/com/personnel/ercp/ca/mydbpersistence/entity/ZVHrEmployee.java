package cn.com.personnel.ercp.ca.mydbpersistence.entity;

public class ZVHrEmployee {

    private String pernr;

    private String ename;

    private String zhrPtext;

    private String deptName;

    private String zhrEmail;

    private String zhrCell;

    private String zhrTell;
    private String zhrStext;
    
    private String flag;
    
    
    
	public String getPernr() {
		return pernr;
	}
	public void setPernr(String pernr) {
		this.pernr = pernr;
	}
	public String getEname() {
		return ename;
	}
	public void setEname(String ename) {
		this.ename = ename;
	}
	public String getZhrPtext() {
		return zhrPtext;
	}
	public void setZhrPtext(String zhrPtext) {
		this.zhrPtext = zhrPtext;
	}
	public String getDeptName() {
		return deptName;
	}
	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}
	public String getZhrEmail() {
		return zhrEmail;
	}
	public void setZhrEmail(String zhrEmail) {
		this.zhrEmail = zhrEmail;
	}
	public String getZhrCell() {
		return zhrCell;
	}
	public void setZhrCell(String zhrCell) {
		this.zhrCell = zhrCell;
	}
	public String getZhrTell() {
		return zhrTell;
	}
	public void setZhrTell(String zhrTell) {
		this.zhrTell = zhrTell;
	}
	
	
	
	public String getZhrStext() {
		return zhrStext;
	}
	public void setZhrStext(String zhrStext) {
		this.zhrStext = zhrStext;
	}
	public String getFlag() {
		return flag;
	}
	public void setFlag(String flag) {
		this.flag = flag;
	}
    
    
   }